#!/bin/bash

if [ "$(id -u)" -ne 0 ]
then
    echo "Ce script doit être exécuté en tant qu'utilisateur root"
    exit 1
fi

generate_certificates() {
    domain=$1
    key_file="${domain}.key"
    csr_file="${domain}.csr"
    crt_file="${domain}.crt"

    openssl req -newkey rsa:4096 -nodes -keyout "${key_file}" -subj "/C=CN/ST=GD/L=SZ/O=Acme, Inc./CN=${domain}" -out "${csr_file}"
    openssl x509 -req -extfile <(printf "subjectAltName=DNS:${domain},DNS:www.${domain}") -days 1365 -in "${csr_file}" -CA ca.crt -CAkey ca.key -CAcreateserial -out "${crt_file}"

    mv "${crt_file}" "${key_file}" "${2}/cert/"
}

openssl genrsa -out ca.key 4096
openssl req -new -x509 -days 365 -key ca.key -subj "/C=CN/ST=GD/L=SZ/O=Acme, Inc./CN=Acme Root CA" -out ca.crt

mkdir -p loki/cert
mkdir -p promtail/cert

generate_certificates "lokiserver.com" "loki"
generate_certificates "promtailclient.com" "promtail"

cp ca.crt loki/cert/
mv ca.crt promtail/cert/

rm -rf ca.key ca.srl *.csr

echo "Done!"
