# LOKI PROMTAIL GRAFANA

Simple stack pour monitorer les logs des conteneurs ou applications d'un serveur via LOKI / PROMTAIL

![ARCHI](docs/promtail-loki.png)

## UTILISATION LOCALE

```bash
docker-compose up -d
```

Grafana est disponible à l'adresse: http://localhost:3000
> user: admin / password: admin

## CONFIGURATION PROD

De base promtail est configuré pour faire remonter les logs systèmes (/var/log)

### PROMTAIL AGENT HOST

Promtail est l'agent qui va pusher les logs vers Loki:
```yml
  promtail:
    image:  grafana/promtail:2.9.4
    container_name: promtail
    volumes:
      - ./promtail/config.yml:/etc/promtail/config.yml
      - /var/lib/docker/containers:/var/lib/docker/containers:ro
      - /var/run/docker.sock:/var/run/docker.sock
      - /var/log:/var/log
    command: -config.file=/etc/promtail/config.yml
```

Pour que LOKI récupère les logs des conteneurs il faut ajouter les labels aux conteneurs dont on veux monitorer les logs (optionnel):
```yml
    labels:
      logging: "promtail"
      logging_jobname: "containerlogs"
```

### GRAFANA

C'est sur le serveur de Grafana que l'on déploie Loki (de préférence):

```yml
version: "3"

services:
  loki:
    image: grafana/loki:2.9.4
    container_name: loki
    ports:
      - 3100:3100
    volumes:
      - ./loki/config:/etc/loki
      - ./loki/cert:/etc/loki/cert
    command: -config.file=/etc/loki/config.yml
```

### TLS 

Les metrics des agents promtail transitent par le WAN et nécessitent d'être encryptées.

- Creation des certificats:

> Renseigner les nom du serveur LOKI ainsi que son DNS, idem pour l'agent Promtail et lançer le script:

```bash
sudo ./certificates.sh
```

#### LOKI

Décommenter les lignes concernant le TLS dans promtail/config/yml comme suit:

```yaml
clients:
  # LOCAL
  # - url: http://loki:3100/loki/api/v1/push
  
  # DISTANT TLS
  - url: https://loki-dns-serveur:3100/loki/api/v1/push
    tls_config:
      ca_file: /etc/promtail/cert/ca.crt
      cert_file: /etc/promtail/cert/promtail.client.crt
      key_file: /etc/promtail/cert/client.key
      server_name: loki-dns-serveur
      insecure_skip_verify: false
```

#### PROMTAIL

Idem pour loki/config/config.yml:

```yaml
server:
  http_listen_port: 3100
  
# DISTANT TLS
  grpc_listen_port: 9096
  http_tls_config:
    cert_file: /etc/loki/cert/loki.server.crt
    key_file: /etc/loki/cert/server.key
    client_auth_type: RequireAndVerifyClientCert
    client_ca_file: /etc/loki/cert/ca.crt
```

#### NGINX

Nginx reverse proxy configuration:

```
upstream loki {
        server 127.0.0.1:3100;
}

server {
    listen 80;
    listen [::]:80;
    server_name loki.mon-domaine.tld;
    rewrite ^(.*) https://loki.mon-domaine.tld$1 permanent;
}

server {
    listen [::]:443 ssl;
    listen 443 ssl;
    server_name loki.mon-domaine.tld;

    client_max_body_size 200M;

    location / {
            proxy_buffering off;
            proxy_pass http://loki;
            proxy_pass_request_headers on;
            proxy_set_header Host $host;
            proxy_set_header X-Real-IP $remote_addr;
    }

    error_log /var/log/nginx/loki.mon-domaine.tld-proxy-error.log;
    access_log /var/log/nginx/loki.mon-domaine.tld-proxy-access.log;

    ssl_certificate           /etc/letsencrypt/live/loki.mon-domaine.tld/fullchain.pem;
    ssl_certificate_key       /etc/letsencrypt/live/loki.mon-domaine.tld/privkey.pem;

    ssl_protocols       TLSv1 TLSv1.1 TLSv1.2 TLSv1.3;
    ssl_ciphers         HIGH:!aNULL:!MD5;
}
```

> Modifier loki-dns-serveur avec le vrai dns du serveur

#### GRAFANA

Configurer:
- l'URI du serveur Loki 
- Le certificat CA (/etc/loki/cert/ca.crt)
- Le certificat client (/etc/loki/cert/server.crt)
- La clé client (/etc/loki/cert/server.key)


![AJOUT DATASOURCE](docs/datasource.png)

## DASHBOARD

> import dashboard ID: 17514
> Faire la dashboard standard

## TO DO

- [X] schema type
- [X] provisionner dashboard
- [X] Promtail config
- [X] TLS config (https)
- [X] SSH logs
